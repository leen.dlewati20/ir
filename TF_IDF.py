from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer()


def generate_documents_vectors(processed_dic):
    return vectorizer.fit_transform(processed_dic.values())


def generate_query_vector(query):
    return vectorizer.transform([query])

