from operator import itemgetter

from sklearn.metrics.pairwise import cosine_similarity

import CISI
import main
documnets = CISI.documents_dic
queries = CISI.queries
mappings = CISI.mappings
doc_vectors = main.documents_matrix.toarray()
qry_vectors = main.queries_matrix.toarray()


def precision():
    rel_ret = []  # relevant & retrieved


def calculate_precision(model_output, gold_standard):
    true_pos = 0
    for item in model_output:
        if item in gold_standard:
            true_pos += 1
    return float(true_pos) / float(len(model_output))


def calculate_found(model_output, gold_standard):
    found = 0
    for item in model_output:
        if item in gold_standard:
            found = 1
    return float(found)


precision_all = 0.0
found_all = 0.0
for query_id in mappings.keys():
    gold_standard = mappings.get(str(query_id))
    query = qry_vectors[int(query_id)-1].reshape(1, -1)
    results = {}
    model_output = []
    doc_id = 1
    for i in doc_vectors:
        document = i.reshape(1, -1)
        # print(query)
        # print("\n")
        # print(document)
        cosine = cosine_similarity(query, document)
        cosine = cosine.tolist()
        cosine = cosine[0][0]
        results[doc_id] = cosine
        doc_id += 1
    for items in sorted(results.items(), key=itemgetter(1), reverse=True)[:10]:
        model_output.append(items[0])
    precision = calculate_precision(model_output, gold_standard)
    found = calculate_found(model_output, gold_standard)
    print(str(query_id) + ": " + str(precision))
    precision_all += precision
    found_all += found

print(precision_all / float(len(mappings.keys())))
print(found_all / float(len(mappings.keys())))
