from numpy import array
from numpy.linalg import norm

import CISI
import TF_IDF
from operator import itemgetter
from PreProcessing import date_processor, convertNumToWords, replaceDash, removePuncs, stemmer, lemmatizer,preProcess
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import pandas as pd

cisi_corpus = CISI.documents_dic
processed_dic= {}
for key, value in cisi_corpus.items():
    value = date_processor(value)
    value = convertNumToWords(value)
    value = replaceDash(value)
    #TODO normalization (lowercasing and u.s.a exmaple)
    value = removePuncs(value)
    value = stemmer(value)
    value = ' '.join(lemmatizer(value))
    processed_dic[key] = value


cisi_queries = CISI.queries
processed_queries_dic = {}
for key, value in cisi_queries.items():
    value = convertNumToWords(value)
    value = replaceDash(value)
    #TODO normalization (lowercasing and u.s.a exmaple)
    value = removePuncs(value)
    value = stemmer(value)
    value = ' '.join(lemmatizer(value))
    processed_queries_dic[key] = value

# Compute the similarities using the word counts
query = "What problems and concerns are there in making up descriptive titles? What difficulties are involved in automatically retrieving articles from approximate titles? What is the usual relevance of the content of articles to their titles?"

# documents_matrix = TF_IDF.generate_documents_vectors(processed_dic)
# queries_matrix = TF_IDF.generate_documents_vectors(processed_queries_dic)


from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer()
documents_matrix = vectorizer.fit_transform(processed_dic)
queries_matrix = vectorizer.transform(processed_queries_dic)

# tfidf = TF_IDF.vectorizer
# feature_names = tfidf.get_feature_names_out()
# corpus_index = [n for n in processed_queries_dic.keys()]
# import pandas as pd
# df = pd.DataFrame(queries_matrix.T.todense(), index=feature_names, columns=corpus_index)
# # print(df)
# print(queries_matrix.toarray()[76])


