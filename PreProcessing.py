# CISI corpus
import string
import CISI

corpus = CISI.documents_dic
# or corpus = CACM.documents_dic depending on the choice of the user

from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.tokenize import RegexpTokenizer

################## merging all documents into a single string ###############
corpus_string = []
for key, value in corpus.items():
    corpus_string.append(value)
corpus_string = ' '.join(corpus_string)

############## date parsing ##############
import datefinder
import calendar

dic_month = {month: index for index, month in enumerate(calendar.month_abbr) if month}


def date_processor(text):
    try:
        match = datefinder.find_dates(text, source=1)

        for item in match:
            replacement = ""

            if (item[1].__len__() > 4) and not ('PM' in item[1]) and not ('AM' in item[1]) and (
                    any(ch.isdigit() for ch in item[1])):
                replacement += str(item[0].day)
                replacement += "/"
                if type(item[0].month) == int:
                    replacement += str(item[0].month)
                else:
                    replacement += dic_month[item[0].month]
                replacement += "/"
                replacement += str(item[0].year)
                text = text.replace(item[1], replacement)

    except:
        pass

    return text


corpus_string2 = date_processor(corpus_string)

###############number 2 words ################
from num2words import num2words


def convertNumToWords(text):
    for i in text.split():
        if i.isdigit():
            word = num2words(i)
            text = text.replace(i, word)
    return text


######### removing punctuations and anything other than letters or numbers #############
tokenizer = RegexpTokenizer(r'\w+')
# corpus_string = corpus_string.replace("-", " ")
punc = tokenizer.tokenize(corpus_string)
# for i, w in enumerate(punc):
#     print("Token " + str(i) + ": " + w)
# print('----------------------------------')
cleaned_text = ' '.join(punc)
#
# # nltk.download("stopwords")
from nltk.corpus import stopwords

#
stop_words = set(stopwords.words('english'))


# filtered_sentence = []

# for w in punc:
#     if w not in stop_words:
#         filtered_sentence.append(w)

# word_list = [word for word in word_tokenize(corpus_string.lower())
#              if not word in stop_words and not word in string.punctuation]

# word_list = ' '.join(word_list)
# print(word_list)
def replaceDash(text):
    text = text.replace("-", " ")
    # text = text.replace("/", " ")
    return text


# my_puncs = string.punctuation.replace("/", "")
def removePuncs(text):
    text = text.translate(str.maketrans('', '', string.punctuation))
    return text


# filtered_sentence = [word for word in word_tokenize(output.lower())]
#              if not word in stop_words and not word in string.punctuation]
# for i, w in enumerate(word_tokenize(output)):
#     print("Token " + str(i) + ": " + w)
# print('----------------------------------')
# print(string.punctuation)
# filtered_sentence_string = ' '.join(filtered_sentence)
# print('stopwords removal: ' + filtered_sentence_string)

# # stemming # #
from nltk.stem import PorterStemmer


def stemmer(text):
    ps = PorterStemmer()
    stemmed_text = ps.stem(text)
    return stemmed_text


# print('stemmed text: ' + stemmed_text)

# # lemmatization # #
# # nltk.download('wordnet')
# # nltk.download('omw-1.4')
from nltk.stem import WordNetLemmatizer
from nltk.tag import pos_tag


#
#
# # nltk.download('averaged_perceptron_tagger')
def lemmatizer(sentence):
    wnl = WordNetLemmatizer()
    for word, tag in pos_tag(word_tokenize(sentence)):
        if tag.startswith("NN"):
            yield wnl.lemmatize(word, pos='n')
        elif tag.startswith('VB'):
            yield wnl.lemmatize(word, pos='v')
        elif tag.startswith('JJ'):
            yield wnl.lemmatize(word, pos='a')
        else:
            yield word


#
#
# lem_text = ' '.join(lemmatize_all(stemmed_text))
# print('lemmatized text: ' + lem_text)

############### auto-correction and suggestion ##############
from textblob import TextBlob
from textblob.en import Spelling


def correct(str):
    textBlb = TextBlob(str)
    correctedText = textBlb.correct()
    return correctedText


import re

textToLower = ""
text = corpus_string
textToLower = text.lower()  # Lower all the capital letters

words = re.findall("[a-z]+", textToLower)  # Find all the words and place them into a list
oneString = " ".join(words)  # Join them into one string

pathToFile = "train.txt"  # The path we want to store our stats file at
spelling = Spelling(path=pathToFile)  # Connect the path to the Spelling object
spelling.train(oneString, pathToFile)


# print(spelling.suggest('develoment'))

# # tf
# bagOfWords = word_tokenize(lem_text)
# wordsDic = dict.fromkeys(bagOfWords, 0)
# for word in bagOfWords:
#     wordsDic[word] += 1
# print(wordsDic)
#
#
# def computeTF(wordDict, bagOfWords):
#     tfDict = {}
#     bagOfWordsCount = len(bagOfWords)
#     for word, count in wordDict.items():
#         tfDict[word] = count / float(bagOfWordsCount)
#     return tfDict
#
#
# tf = computeTF(wordsDic, bagOfWords)
# print(tf)
#
#
# # idf
# def computeIDF(documents):
#     import math
#     N = len(documents)
#
#     idfDict = dict.fromkeys(documents[0].keys(), 0)
#     for document in documents:
#         for word, val in document.items():
#             if val > 0:
#                 idfDict[word] += 1
#
#     for word, val in idfDict.items():
#         idfDict[word] = math.log(N / float(val))
#     return idfDict
#
#
# idfs = computeIDF([wordsDic])
# print(idfs)
#
#
# # tf-idf
# def computeTFIDF(tfBagOfWords, idfs):
#     tfidf = {}
#     for word, val in tfBagOfWords.items():
#         tfidf[word] = val * idfs[word]
#     return tfidf
#
#
# tfidf = computeTFIDF(tf, idfs)
# print(tfidf)


def preProcess(text):
    text = convertNumToWords(text)
    text = replaceDash(text)
    text = removePuncs(text)
    text = stemmer(text)
    text = ' '.join(lemmatizer(text))
    return text
