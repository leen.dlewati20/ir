from tkinter import *

root = Tk()

# Label(root, text="").pack()
Label(root, text="Choose a dataset: ").pack(anchor=W)

option = IntVar()
Radiobutton(root, text="A", variable=option, value=1,).place(x=100, y=22)
"""command=lambda: click(option.get())"""
Radiobutton(root, text="B", variable=option, value=2, ).place(x=100, y=50)

Button(root, text="Ok", padx=50, command=lambda :searchWin()).place(x=150, y=80)

def click(o):
    Label(root, text=o).pack(anchor=W)


def searchWin():
    new_win = Toplevel(root)
    Label(new_win, text='Enter your query:').pack(anchor=W)
    new_win.geometry("600x300")
    q = Entry(new_win, width=70, borderwidth=5)
    q.pack()
    Button(new_win, text="search", padx=10, command=lambda: search()).place(x=520, y=20)
    Button(new_win, text="close", padx=40, command=lambda: new_win.destroy()).place(x=450, y=250)
    def search():
        Label(new_win, text=q.get()).pack(anchor=W)


root.geometry("300x150")
root.title("Welcome To IR System")
root.mainloop()