# Reading CISI Dataset
def read_documents():
    file = open("CISI/CISI.ALL")
    # new_lines merge each identifier and it's content in a single line
    new_lines = ""

    for line in file.readlines():
        if line.startswith("."):
            new_lines += "\n" + line.strip()
        else:
            new_lines += " " + line.strip()

    documents_dic = {}
    cross_references = {}

    content = ""
    doc_id = ""

    for line in new_lines.split("\n"):
        if line.startswith(".I"):
            # [0] is .I [1] is the id
            doc_id = line.split(" ")[1].strip()
        elif line.startswith(".X"):
            # arr = line.strip()[3:].split()
            # cross_references[doc_id] = arr
            documents_dic[doc_id] = content
            content = ""
            doc_id = ""
        else:
            # [0] = . ,[1] = identifier, [2] = space, [3] = start of text
            content += line.strip()[3:] + " "
    file.close()
    return documents_dic, cross_references


documents_dic = read_documents()[0]
cross_reference_dic = read_documents()[1]

# print(len(documents_dic))
# print(documents_dic.get("1"))
# i=0
# for key, value in documents_dic.items():
#     print(f"{key}: {value}")
#     i+=1
#     if(i==3):
#         break
# print(cross_reference_dic["1"])


def read_queries():
    file = open("CISI/CISI.QRY")
    merged = ""

    for a_line in file.readlines():
        if a_line.startswith("."):
            merged += "\n" + a_line.strip()
        else:
            merged += " " + a_line.strip()

    queries = {}

    content = ""
    qry_id = ""

    for a_line in merged.split("\n"):
        if a_line.startswith(".I"):
            if not content == "":
                queries[qry_id] = content
                content = ""
                # qry_id = ""
            qry_id = a_line.split(" ")[1].strip()
        elif a_line.startswith(".W") or a_line.startswith(".T"):
            # [0] = . ,[1] = identifier, [2] = space, [3] = start of text
            content += a_line.strip()[3:] + " "
    queries[qry_id] = content
    file.close()
    return queries


queries = read_queries()


def golden_standard():
    f = open("cisi/CISI.REL")

    mappings = {}

    for a_line in f.readlines():
        voc = a_line.strip().split()
        key = voc[0].strip()
        current_value = voc[1].strip()
        value = []
        if key in mappings.keys():
            value = mappings.get(key)
        value.append(current_value)
        mappings[key] = value

    f.close()
    return mappings


mappings = golden_standard()  # mapping dictionary holds query_ids as keys
# and the values are the docs_id relevant to this query



